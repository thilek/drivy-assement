# Drivy Android Assessment

The requirement for the assessment can be found at [following link](https://github.com/drivy/jobs/blob/master/android/README.md)

This assessment was done with [Model View Presenter](https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93presenter) architectural pattern.
The purpose was to keep the business logic out-side from View and enable those logic to be easily testable in unit test.


## Third Party Libraries Used.
- [Picasso](https://square.github.io/picasso/) for image downloading as this library handles downloading and caching in the background thread. This keeps the UI very responsive.
- [Retrofit](https://square.github.io/retrofit/) for network calls. Retrofit simply the best for network handling. Lots of flexibilities with interceptors and more.
- [ButterKnife](https://jakewharton.github.io/butterknife/) for view injection. I believe Android has view binding but I'm used to ButterKnife which reduces view declarations.

## Some implementation decisions.
I used custom view over using the views directly in the activity. I like to keep the activities as small as possible with minimum code and logic inside.
By creating custom views, i'm able to reuse the views anywhere else in the app and also keep all the logic related to the view inside the view.

I didn't resize the image on the list and kept to fit the view without cropping or stretching the image. But of coz if it's required, can use
other scale types which fits the requirements by design team. Another approach is to use grid in recyclerview to fit the images dimension ratio. This will allows us to show more
items with less scrolling.

I didn't implement unit test as there were not many testable logic inside this assessment. I should have created unit tests for the presenter,
Unfortunately I used Android Async task to run the API calls in background. Therefor I couldn't do unit test on the presenter.
This was done to keep this assessment simple and fast to implement. Alternatively, we could use RxJava to replace Async task.
RxJava provides lots of useful operators for more complex logic.


## Some future improvements.
- Implement sorting of the list with certain parameters such as price or ratings. Or option for the user to sort with few options.
- Search and filter by model or price.
- I would like to use espresso UI test but I had limited time to complete this assessment. If I had the chance to do it, I would use mock-server or created interceptor for retrofit, 
which would provide mocked responses. With mocked responses, i should be able to test different scenarios and responses and test the views.