package com.thilek.android.drivy.service.model;

import com.google.gson.annotations.SerializedName;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;

public class Owner {

    @SerializedName("name")
    private final String name;
    @SerializedName("rating")
    private final Rating rating;
    @SerializedName("picture_url")
    private final String picture;

    public Owner(@NotNull String name, @NotNull Rating rating, @Nullable String picture) {
        this.name = name;
        this.rating = rating;
        this.picture = picture;
    }

    @NotNull
    public String getName() {
        return name;
    }

    @NotNull
    public Rating getRating() {
        return rating;
    }

    @Nullable
    public String getPicture() {
        return picture;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Owner owner = (Owner) o;
        return Objects.equals(name, owner.name) &&
                Objects.equals(rating, owner.rating) &&
                Objects.equals(picture, owner.picture);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, rating, picture);
    }

}
