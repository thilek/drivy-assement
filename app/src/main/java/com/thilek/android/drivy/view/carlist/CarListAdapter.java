package com.thilek.android.drivy.view.carlist;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.core.util.Pair;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.thilek.android.drivy.CarDetailActivity;
import com.thilek.android.drivy.R;
import com.thilek.android.drivy.service.model.Car;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CarListAdapter extends RecyclerView.Adapter<CarListAdapter.ViewHolder> {

    private List<Car> cars = new ArrayList<>();

    @NotNull
    @Override
    public CarListAdapter.ViewHolder onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.car_item, parent, false);
        return new ViewHolder(view);
    }

    private Car getItem(int position) {
        return cars.get(position);
    }

    @Override
    public void onBindViewHolder(@NotNull ViewHolder holder, int position) {
        holder.setData(getItem(position));
    }

    @Override
    public int getItemCount() {
        return cars.size();
    }

    void setCars(List<Car> car) {
        this.cars = car;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.car_model_view)
        TextView carModelView;
        @BindView(R.id.car_price_view)
        TextView carPriceView;
        @BindView(R.id.car_rating_count_view)
        TextView carRatingCountView;
        @BindView(R.id.car_rating_view)
        RatingBar carRatingView;
        @BindView(R.id.car_image_view)
        ImageView carImageView;

        private Car car;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        @OnClick(R.id.car_item_view)
        public void onItemClicked(View view) {
            if (car != null) {
                CarDetailActivity.openCarDetailActivity(view.getContext(), car);
            }
        }

        void setData(Car car) {
            this.car = car;
            carModelView.setText(String.format(carPriceView.getContext().getString(R.string.car_list_title), car.getBrand(), car.getModel()));
            carPriceView.setText(String.format(carPriceView.getContext().getString(R.string.car_list_price), car.getPrice().toString()));
            carRatingCountView.setText(String.valueOf(car.getRating().getCount()));
            carRatingView.setRating((float) car.getRating().getAverage());
            Picasso.get().load(car.getPicture()).into(carImageView);
        }

    }
}