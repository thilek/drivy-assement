package com.thilek.android.drivy.service;

import com.thilek.android.drivy.service.model.Car;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public interface CarDataService {

    @NotNull
    List<Car> getListOfCars() throws CarDataException;

}
