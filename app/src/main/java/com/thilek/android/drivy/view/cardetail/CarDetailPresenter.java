package com.thilek.android.drivy.view.cardetail;

import android.os.Bundle;

import com.google.gson.Gson;
import com.thilek.android.drivy.CarDetailActivity;
import com.thilek.android.drivy.service.model.Car;

import org.jetbrains.annotations.NotNull;

final class CarDetailPresenter {

    private final CarDetailView view;

    CarDetailPresenter(CarDetailView view) {
        this.view = view;

    }

    void getCarData(@NotNull Bundle bundle) {
        if (bundle.containsKey(CarDetailActivity.BUNDLE_CAR_DATA)) {
            Gson gson = new Gson();
            Car car = gson.fromJson(bundle.getString(CarDetailActivity.BUNDLE_CAR_DATA), Car.class);
            view.showCarDetail(car);
            view.showOwnerDetail(car.getOwner());
        }

    }


}
