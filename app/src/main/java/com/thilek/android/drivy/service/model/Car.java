package com.thilek.android.drivy.service.model;

import com.google.gson.annotations.SerializedName;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.math.BigDecimal;
import java.util.Objects;

public final class Car {

    @SerializedName("brand")
    private final String brand;
    @SerializedName("model")
    private final String model;
    @SerializedName("picture_url")
    private final String picture;
    @SerializedName("price_per_day")
    private final BigDecimal price;
    @SerializedName("rating")
    private final Rating rating;
    @SerializedName("owner")
    private final Owner owner;

    public Car(@NotNull String brand, @NotNull String model, @Nullable String picture, @NotNull BigDecimal price, @NotNull Rating rating, @NotNull Owner owner) {
        this.brand = brand;
        this.model = model;
        this.picture = picture;
        this.price = price;
        this.rating = rating;
        this.owner = owner;
    }

    @NotNull
    public String getBrand() {
        return brand;
    }

    @NotNull
    public String getModel() {
        return model;
    }

    @Nullable
    public String getPicture() {
        return picture;
    }

    @NotNull
    public BigDecimal getPrice() {
        return price;
    }

    @NotNull
    public Rating getRating() {
        return rating;
    }

    @NotNull
    public Owner getOwner() {
        return owner;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return Objects.equals(brand, car.brand) &&
                Objects.equals(model, car.model) &&
                Objects.equals(picture, car.picture) &&
                Objects.equals(price, car.price) &&
                Objects.equals(rating, car.rating) &&
                Objects.equals(owner, car.owner);
    }

    @Override
    public int hashCode() {
        return Objects.hash(brand, model, picture, price, rating, owner);
    }

}
