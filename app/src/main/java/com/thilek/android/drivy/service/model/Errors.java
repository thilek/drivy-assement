package com.thilek.android.drivy.service.model;

public enum Errors {
    SERVER_INTERNAL_ERROR,
    INVALID_RESPONSE,
    UNKNOWN
}
