package com.thilek.android.drivy.view.carlist;

import android.os.AsyncTask;

import com.thilek.android.drivy.service.CarDataException;
import com.thilek.android.drivy.service.CarDataService;
import com.thilek.android.drivy.service.model.Car;

import java.util.List;

final class CarListPresenter {

    private final CarListView view;
    private final CarDataService carDataService;
    private FetchingTask fetchingTask;

    CarListPresenter(CarDataService carDataService, CarListView view) {
        this.view = view;
        this.carDataService = carDataService;
    }

    void fetchListOfCars() {
        getFetchingTask().execute();
    }

    private FetchingTask getFetchingTask() {
        if (fetchingTask == null) {
            fetchingTask = new FetchingTask(carDataService, view);
        } else {
            if (fetchingTask.getStatus() != AsyncTask.Status.RUNNING) {
                fetchingTask.cancel(true);
            }
            fetchingTask = new FetchingTask(carDataService, view);
        }

        return fetchingTask;
    }

    private static class FetchingTask extends AsyncTask<Void, Integer, List<Car>> {

        private final CarDataService carDataService;
        private final CarListView view;

        FetchingTask(CarDataService carDataService, CarListView view) {
            this.carDataService = carDataService;
            this.view = view;
        }

        @Override
        protected List<Car> doInBackground(Void... voids) {

            try {
                return carDataService.getListOfCars();
            } catch (CarDataException e) {
                view.showErrorMessage(e.getError());
            }

            return null;
        }

        @Override
        protected void onPostExecute(List<Car> cars) {
            super.onPostExecute(cars);
            if (cars != null) {
                view.showCarList(cars);
            }
        }
    }

}
