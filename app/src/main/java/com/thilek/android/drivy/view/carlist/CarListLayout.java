package com.thilek.android.drivy.view.carlist;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.thilek.android.drivy.R;
import com.thilek.android.drivy.service.CarApiService;
import com.thilek.android.drivy.service.model.Car;
import com.thilek.android.drivy.service.model.Errors;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CarListLayout extends RelativeLayout implements CarListView, SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.car_list_recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.swipe_container)
    SwipeRefreshLayout swipeRefreshLayout;

    private CarListAdapter listAdapter;
    private CarListPresenter presenter;
    private AlertDialog alertDialog;

    public CarListLayout(Context context) {
        super(context);
        buildLayout();
    }

    public CarListLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        buildLayout();
    }

    private void buildLayout() {
        View view = inflate(getContext(), R.layout.car_list_layout, this);
        ButterKnife.bind(view);

        presenter = new CarListPresenter(new CarApiService(), this);

        swipeRefreshLayout.setOnRefreshListener(this);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this.getContext()));

        LayoutAnimationController animationController = AnimationUtils.loadLayoutAnimation(getContext(), R.anim.layout_animation_fall_down);
        recyclerView.setLayoutAnimation(animationController);

        listAdapter = new CarListAdapter();
        recyclerView.setAdapter(listAdapter);

    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        retrieveCarList();
    }

    @Override
    public void onRefresh() {
        retrieveCarList();
    }

    @Override
    public void retrieveCarList() {
        if (!swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(true);
        }

        listAdapter.setCars(new ArrayList<>());
        listAdapter.notifyDataSetChanged();

        presenter.fetchListOfCars();
    }

    @Override
    public void showCarList(List<Car> cars) {
        if (swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
        }

        listAdapter.setCars(cars);
        listAdapter.notifyDataSetChanged();
        recyclerView.scheduleLayoutAnimation();
    }

    @Override
    public void showErrorMessage(Errors message) {
        this.post(() -> {
            if (swipeRefreshLayout.isRefreshing()) {
                swipeRefreshLayout.setRefreshing(false);
            }

            alertDialog = new AlertDialog.Builder(CarListLayout.this.getContext())
                    .setMessage(R.string.car_list_dialog_error_message)
                    .setPositiveButton(R.string.generic_dialog_yes, (dialog, which) -> {
                        retrieveCarList();
                    })
                    .setNegativeButton(R.string.generic_dialog_no, (dialog, which) -> {
                        if (alertDialog != null) {
                            alertDialog.dismiss();
                        }
                    })
                    .create();
            alertDialog.show();
        });
    }
}
