package com.thilek.android.drivy.service.model;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

public class Rating {

    @SerializedName("average")
    private final double average;
    @SerializedName("count")
    private final int count;

    public Rating(double average, int count) {
        this.average = average;
        this.count = count;
    }

    public double getAverage() {
        return average;
    }

    public int getCount() {
        return count;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Rating rating = (Rating) o;
        return Double.compare(rating.average, average) == 0 &&
                count == rating.count;
    }

    @Override
    public int hashCode() {
        return Objects.hash(average, count);
    }
}
