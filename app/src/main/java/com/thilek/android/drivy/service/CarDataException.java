package com.thilek.android.drivy.service;

import com.thilek.android.drivy.service.model.Errors;

public class CarDataException extends Exception {

    private final Errors error;

    public CarDataException(Errors error) {
        this.error = error;
    }

    public Errors getError() {
        return error;
    }
}
