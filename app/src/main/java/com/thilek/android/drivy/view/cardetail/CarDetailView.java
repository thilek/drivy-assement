package com.thilek.android.drivy.view.cardetail;

import android.os.Bundle;

import com.thilek.android.drivy.service.model.Car;
import com.thilek.android.drivy.service.model.Owner;

public interface CarDetailView {

    void retrieveCarData(Bundle bundle);

    void showOwnerDetail(Owner owner);

    void showCarDetail(Car car);

}
