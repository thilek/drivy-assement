package com.thilek.android.drivy.view.carlist;

import com.thilek.android.drivy.service.model.Car;
import com.thilek.android.drivy.service.model.Errors;

import java.util.List;

public interface CarListView {

    void retrieveCarList();

    void showCarList(List<Car> cars);

    void showErrorMessage(Errors message);
}
