package com.thilek.android.drivy;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.thilek.android.drivy.service.model.Car;
import com.thilek.android.drivy.view.cardetail.CarDetailLayout;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CarDetailActivity extends AppCompatActivity {

    public static final String BUNDLE_CAR_DATA = "car_data";

    @BindView(R.id.car_detail_layout)
    CarDetailLayout carDetailLayout;

    public static void openCarDetailActivity(Context context, Car car) {
        Gson gson = new Gson();

        Intent intent = new Intent(context, CarDetailActivity.class);
        intent.putExtra(BUNDLE_CAR_DATA, gson.toJson(car));

        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.car_detail_activity);
        ButterKnife.bind(this);

        carDetailLayout.retrieveCarData(getIntent().getExtras());
    }

}
