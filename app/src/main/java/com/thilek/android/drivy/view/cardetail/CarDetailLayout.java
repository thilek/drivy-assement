package com.thilek.android.drivy.view.cardetail;

import android.content.Context;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.thilek.android.drivy.R;
import com.thilek.android.drivy.service.model.Car;
import com.thilek.android.drivy.service.model.Owner;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CarDetailLayout extends RelativeLayout implements CarDetailView {

    @BindView(R.id.car_model_view)
    TextView carModelView;

    @BindView(R.id.car_price_view)
    TextView carPriceView;

    @BindView(R.id.car_rating_count_view)
    TextView carRatingCountView;

    @BindView(R.id.car_rating_view)
    RatingBar carRatingView;

    @BindView(R.id.car_image_view)
    ImageView carImageView;

    @BindView(R.id.car_detail_view)
    View carDetailView;

    @BindView(R.id.owner_detail_view)
    View ownerDetailView;

    @BindView(R.id.owner_name_view)
    TextView ownerNameView;

    @BindView(R.id.owner_image_view)
    ImageView ownerImageView;

    @BindView(R.id.owner_rating_view)
    RatingBar ownerRatingView;

    private CarDetailPresenter carDetailPresenter;

    public CarDetailLayout(Context context) {
        super(context);
        buildLayout();
    }

    public CarDetailLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        buildLayout();
    }

    private void buildLayout() {
        View view = inflate(getContext(), R.layout.car_detail_layout, this);
        ButterKnife.bind(view);

        carDetailPresenter = new CarDetailPresenter(this);
    }

    @Override
    public void retrieveCarData(Bundle bundle) {
        carDetailPresenter.getCarData(bundle);
    }

    @Override
    public void showOwnerDetail(Owner owner) {
        ownerNameView.setText(owner.getName());
        ownerRatingView.setRating((float) owner.getRating().getAverage());
        Picasso.get().load(owner.getPicture()).resize(64,56).centerInside().into(ownerImageView);

        carDetailView.setVisibility(VISIBLE);
    }

    @Override
    public void showCarDetail(Car car) {
        carModelView.setText(String.format(getContext().getString(R.string.car_list_title), car.getBrand(), car.getModel()));
        carPriceView.setText(String.format(getContext().getString(R.string.car_list_price), car.getPrice().toString()));
        carRatingCountView.setText(String.valueOf(car.getRating().getCount()));
        carRatingView.setRating((float) car.getRating().getAverage());
        Picasso.get().load(car.getPicture()).fit().into(carImageView);

        ownerDetailView.setVisibility(VISIBLE);
    }
}
