package com.thilek.android.drivy.service;

import com.thilek.android.drivy.service.model.Car;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface CarsApi {

    @GET("drivy/jobs/master/android/api/cars.json")
    Call<List<Car>> getCars();

}
