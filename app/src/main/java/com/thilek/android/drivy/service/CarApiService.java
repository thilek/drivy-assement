package com.thilek.android.drivy.service;

import com.thilek.android.drivy.service.model.Car;
import com.thilek.android.drivy.service.model.Errors;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CarApiService implements CarDataService {

    private static final String BASE_URL = "https://raw.githubusercontent.com/";
    private CarsApi carsApi;

    public CarApiService() {
        Retrofit retrofit = new retrofit2.Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        carsApi = retrofit.create(CarsApi.class);
    }

    @Override
    public @NotNull List<Car> getListOfCars() throws CarDataException {

        Call<List<Car>> call = carsApi.getCars();
        try {
            Response<List<Car>> response = call.execute();

            if (response.isSuccessful()) {
                if (response.body() != null) {
                    return response.body();
                } else {
                    throw new CarDataException(Errors.INVALID_RESPONSE);
                }
            } else {
                throw new CarDataException(Errors.SERVER_INTERNAL_ERROR);
            }
        } catch (IOException e) {
            throw new CarDataException(Errors.UNKNOWN);
        }

    }

}
